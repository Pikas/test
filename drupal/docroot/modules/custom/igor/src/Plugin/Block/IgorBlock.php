<?php

namespace Drupal\igor\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Igor' Block.
 *
 * @Block(
 *   id = "igor_block",
 *   admin_label = @Translation("Igor block"),
 *   category = @Translation("Igor block"),
 * )
 */
class IgorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $data = '';

    if (!empty($config['heading'])) {
      $data .= $config['heading'];
    }
    if (!empty($config['copy'])) {
      $data .= $config['copy']['value'];
    }

    return [
      '#markup' => $this->t('@data', [
        '@data' => $data,
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading'),
      '#default_value' => isset($config['heading']) ? $config['heading'] : '',
      '#maxlength' => 100
    ];

    $form['copy'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Copy'),
      '#description' => $this->t('Copy'),
      '#default_value' => isset($config['copy']['value']) ? $config['copy']['value'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['heading'] = $values['heading'];
    $this->configuration['copy'] = $values['copy'];
  }

}
